$(function () {
  /* Get each nav element's width and set it as attribute to be used later */
  submenuLinksSetAttribute()
  /* Display dropdown in left/right based on windows size */
  submenuDropdownPosition()

  submenuCollapse()
  /* When window is resized, run our collapse function */
  $(window).resize(submenuCollapse)

  /* On scroll, call submenuAddShadow */
  $(document).scroll(submenuAddShadow)
})

/* ==========================================================================
  Menu collapse function
  ========================================================================== */
/* Maximum number of elements allowed in the menu  */

/*
  by default the limit is 7, unless an argument is passed.
  This is necessary to unit test this component
*/
const submenuCollapse = (limit = 7) => {
  const submenuLimit = limit
  const submenuLinks = $('.js-submenu-visible a')
  const submenuMore = $('.js-submenu-more')

  let navWidth = 0
  const submenuMoreContent = $('.js-submenu-more-list')
  const submenuWidth = $('.js-submenu-section').width()
  let submenuLinksCheck

  /* Cleanup dom when resizing */
  submenuMoreContent.html('')

  /* 'smart' responsiveness */
  submenuLinks.each(function (index) {
    // Use the width data attr we setup before
    navWidth += $(this).data('width')

    // Possible combination of elements
    const moreSearchWidth = $('.js-submenu-more').outerWidth() + $('.submenu-search').outerWidth()
    const moreUserWidth = $('.js-submenu-more').outerWidth() + $('.user-submenu').outerWidth()
    const combinedWidth = $('.js-submenu-more').outerWidth() + $('.submenu-search').outerWidth() + $('.user-submenu').outerWidth()

    // Cache conditions for readability
    const condLimit = index > submenuLimit - 1
    // Default condition (no search or user)
    const condDefault = navWidth > submenuWidth - 70
    // More dropdown & search
    const condMoreSearch = navWidth > submenuWidth - moreSearchWidth
    // More dropdown & user
    const condMoreUser = navWidth > submenuWidth - moreUserWidth
    // All dropdowns
    const condCombined = navWidth > submenuWidth - combinedWidth

    // If any of these conditions are met
    if (condLimit || condDefault || condMoreSearch || condMoreUser || condCombined) {
      // Hide the nav item
      $(this).addClass('hide')
      // Clone it, strip it from any class and move it to the dropdown
      $(this).clone().removeClass().appendTo(submenuMoreContent)
      // Show the dropdown
      submenuMore.show()
      submenuLinksCheck = true
    } else {
      // If none of these conditions are met, ensure that we show the nav items
      $(this).removeClass('hide')
    }
  })

  // If the amount of nav items is greater than 7 or check if all conditions are met, display the dropdown
  if (submenuLinks.length > submenuLimit || submenuLinksCheck) {
    submenuMore.show()
  } else {
    submenuMore.hide()
  }
}

// If the width of main-submenu is greater than 200px show right dropdown menu
const submenuDropdownPosition = () => {
  if ($('.js-submenu-visible').outerWidth() >= 480) {
    $('.js-submenu-more ul').removeClass('dropdown-menu-left').addClass('dropdown-menu-right')
  }
}

/* Add data-width attr to navigation elements  */
function submenuLinksSetAttribute () {
  $('.js-submenu-visible a').each(function () {
    $(this).attr('data-width', $(this).outerWidth())
  })
}

/* Add shadow to submenu element on scrop */
const submenuAddShadow = () => {
  if ($(window).scrollTop() > 100) {
    $('.js-submenu-section').stop().addClass('submenu-scroll')
  } else {
    $('.js-submenu-section').stop().removeClass('submenu-scroll')
  }
}
