/**
 * Count how many icons are displayed on a page
 *
 * Example of usage:
 *
 * p
 *   | Total icons:
 *   span.js-count-icons
 *
 * .icons-list
 *   article
 *     ...
 *
 * the script will count the number of `article` under .icon-list and place the number in `.js-count-icons`
 */

$(function () {
  const _counterContainer = $('.js-count-icons')
  const _iconsContainer = $('.icons-list article')
  const countIcons = _iconsContainer.length
  _counterContainer.html(countIcons)
})
